-- Possible types are 'NORMAL', 'ACTIVE_READ', 'COMMAND'
local function _addRegister(name, type, getF, setF)
  local reg = {}
  reg.type = type
  reg.getF = getF
  reg.setF = setF
  reg.onChangeCallbacks = {}

  if registers == nil then
    registers = {}
  end
  registers[name] = reg
end

return _addRegister