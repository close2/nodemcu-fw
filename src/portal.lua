local _utils = require('utils')

local _moduleName = 'portal'

-- This function could for instance look if a button is pressed.
local function forceStart() return false end

return function(onConnect)
  package.loaded['utils'] = nil
  local c = _utils.doFile('load_config')(_moduleName)
  _utils = nil

  if c.mode == 'START' or forceStart then
    enduser_setup.start(
    function()
      print("Connected to wifi as:" .. wifi.sta.getip())
      onConnect()
    end,
    function(err, str)
      print('enduser_setup: Err #' .. err .. ': ' .. str)
      print('Restarting in 3 seconds...')
      if not tmr.create():alarm(3000, tmr.ALARM_SINGLE, node.restart) then
        node.restart()
      end
    end,
    print -- Lua print function can serve as the debug callback
    );
  else
    print('Not starting portal.')
    wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, onConnect)
    wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, function(args)
      print("Lost connectivity! Restarting in 3 seconds...")
      if not tmr.create():alarm(3000, tmr.ALARM_SINGLE, node.restart) then
        node.restart()
      end
    end)
  end
end
