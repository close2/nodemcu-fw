local _utils = require('utils')
local _mdnsClient = require('mdns_client')

local _moduleName = 'mqtt_smarthome'

local _clientName
local _m

local function _buildTopicNames(name)
  local sub = _clientName .. '/set/' .. name
  local pub = _clientName .. '/status/' .. name
  local cmd = _clientName .. '/' .. name
  return sub, pub, cmd
end

local function _updateRegister(registerName, newValue)
  print('Update register: ' .. registerName .. ' to ' .. newValue)
  local _, pubTopic = _buildTopicNames(registerName)
  _m:publish(pubTopic, newValue, 0, 1)
end

local function _addRegisters()
  for regName, reg in pairs(registers) do
    if reg.type == 'NORMAL' then
      local cb = function(newVal) _updateRegister(regName, newVal) end
      local regCbs = reg.onChangeCallbacks
      regCbs[#regCbs + 1] = cb
    else
      print('Register ' .. regName .. ' is not type NORMAl, ignoring.')
    end
  end
end

local function _onConnect()
  print ('Connected to mqtt server.')
  local topics = {}

  for name, reg in pairs(registers) do
    local sub = _buildTopicNames(name)
    print('Topic: ' .. sub .. ' will be subscribed to.')
    topics[sub] = 0
  end

  _m:subscribe(topics, function(client)
    print('Subscription to topics successful') end)

  -- Send initial value.
  for registerName, reg in pairs(registers) do
    _updateRegister(registerName, reg.getF())
  end

  _m:publish(_clientName .. '/connected', '2', 1, 1)
end

local function _init(clientName, server, port, secure, user, password)
  print('Will now add callback to registers and subscribe.')
  _addRegisters()

  -- Init mqtt client with logins, keepalive timer 60sec.
  print('Creating new mqttClient ' .. _clientName .. ' user: ' .. user .. ' password: ' .. password)
  _m = mqtt.Client(_clientName, 60, user, password)

  -- Setup Last Will and Testament
  -- Broker will publish a message with qos = 1, retain = 1, data = '0'
  -- to topic _clientName .. '/connected' if client doesn't send keepalive packet
  _m:lwt(_clientName .. '/connected', '0', 1, 1)

  -- on publish message receive event
  _m:on('message', function(client, topic, data)
    print('message received: ' .. topic .. ':' .. data or '')
    for name, reg in pairs(registers) do
      local sub = _buildTopicNames(name)
      if reg.type == 'NORMAL' and sub == topic then
        reg.setF(data)
        break
      end
      -- TODO add command type
    end
  end)


  local _connect

  local function _onDisconnect(client, errorCode)
    local errorStr = _utils.doFile('mqtt_errors')[errorCode]
    print('Mqtt disconnected: (' .. errorCode .. ') → ' .. errorStr)
    tmr.create():alarm(10 * 1000, tmr.ALARM_SINGLE, _connect)
  end

  _connect = function()
    -- For TLS: m:connect(server, secure-port, 1)
    print('Will now try to connect to the server ' .. server .. ':' .. port .. (secure and ' secure' or ' unsecure'))
    _m:connect(server, port, secure, 0, _onConnect, _onDisconnect)
  end

  _connect()
end

local function _startMqttSmarthome(thenF)
  package.loaded['mdns_client'] = nil
  package.loaded['utils'] = nil
  local c = _utils.doFile('load_config')(_moduleName)

  _clientName = c.id

  local secure = (c.secure:lower() == 'true') and 1 or 0
  local port = tonumber(c.port)
  local server = c.server
  local user = c.user
  local password = c.password

  local endsWith = function(s, endS)
    return s ~= nil and endS == '' or string.sub(s, -string.len(endS)) == endS
  end

  if endsWith(server, '.local') then
    print('Will try to resolve server address: ' .. server .. ' using mdns.')

    local _resolvedCb = function(error, queryResult)
      _mdnsClient = nil
      collectgarbage()
      if (queryResult ~= nil and #queryResult > 0) then
        local serverIp, serverPort = _mdnsClient.extractIpAndPortFromResults(queryResult, 1)
        _init(clientName, serverIp, serverPort or port, secure, user, password)
      else
        print('Could not find an mqtt server in local network.' .. (error or ''))
        return thenF and thenF() or nil
      end
      collectgarbage()
      return thenF and thenF() or nil
    end

    return _mdnsClient.query(server, 5, wifi.sta.getip(), _resolvedCb)
  else
    _init(_clientName, server, port, secure, user, password)
    return thenF and thenF() or nil
  end
end

return function()
  return _startMqttSmarthome
end
