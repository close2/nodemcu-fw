local _utils = require('utils')

do
  package.loaded['utils'] = nil
  local registers = { 'led' }
  _utils.doFile('app_launcher')(registers)
end
collectgarbage()