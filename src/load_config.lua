local _utils = require('utils')

local function _read(f)
  local decoder = sjson.decoder()
  local c = file.open(f)
  local line
  repeat
    line = c:read()
    if line then
      decoder:write(line)
    end
  until line == nil
  return decoder:result() or {}
end

local function _load(moduleName)
  package.loaded['utils'] = nil
  local defaultN = 'conf/' .. moduleName
  local userF =    'conf/' .. moduleName .. '.json'

  if not _utils.fileExists(defaultN) then
    print('Configfile: ' .. defaultN .. ' does not exist')
    return {}
  end

  local config = _utils.doFile(defaultN)
  if not file.exists(userF) then
    return config
  end

  -- Apply user settings.
  local userC = _read(userF)
  for c, v in pairs(userC) do
    config[c] = v
  end

  return config
end

return _load