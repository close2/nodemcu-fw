local _utils = require('utils')

return function(projectStartFiles)
  package.loaded['utils'] = nil
  -- Build array of files in reverse order.
  -- It's easier to remove an element at the end of an array.
  local startFiles = { 'http_smarthome', 'mqtt_smarthome', 'mdns_announce' }
  local i = #projectStartFiles
  while i > 0 do
    startFiles[#startFiles + 1] = projectStartFiles[i]
    i = i - 1
  end

  local function _onConnect()
    wifi.setmode(wifi.STATION)
    local function doStartFile()
      while #startFiles > 0 do
        do
          local currentFile = startFiles[#startFiles]
          print('Will now start ' .. currentFile)
          startFiles[#startFiles] = nil
          -- If a startFile returns not nil, it must be a function, which
          -- expects a function as argument.  This function will be executed,
          -- when the startFile has finished.  This is necessary if the
          -- startFile itself uses callback functionality.
          local ret = _utils.doFile(currentFile)()
          if (ret ~= nil) then
            print(currentFile .. ' is a future.')
            return ret(doStartFile)
          end
          print(currentFile .. ' has finished.')
        end
        collectgarbage()
      end
    end
    doStartFile()
  end

  local function _delayOnConnect()
    print('Will now delay calling our onConnect CB for 11 seconds.')
    print('This will give the portal time to shut down.')
    if not tmr.create():alarm(11000, tmr.ALARM_SINGLE, _onConnect) then
      print('Could not delay _onConnect')
    end
  end

  print('Will now start portal')
  _utils.doFile('portal')(_delayOnConnect)
  _utils = nil
end
