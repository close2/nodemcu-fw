local _utils = require('utils')

local _moduleName = 'led'

local _regName = 'on-board/led'
local _pin = 4 -- GPIO2

local _currentVal

local function _onChange(newValue)
  for _, cb in pairs(registers[_regName].onChangeCallbacks) do
    cb(newValue)
  end
end

local function _get()
  return (_currentVal == gpio.HIGH) and '0' or '1'
end

local function _set(newVal)

  print('Changing pin: ' .. _pin or 'nil' ..
        ' from ' .. _currentVal or 'nil' ..
        ' to ' .. newVal or 'nil')

  local newGpioVal = _currentVal
  if newVal == '0' then
    newGpioVal = gpio.HIGH
  elseif newVal == '1' then
    newGpioVal = gpio.LOW
  end

  if newGpioVal ~= _currentVal then
    gpio.write(_pin, newGpioVal)
    _currentVal = newGpioVal
    _onChange(_get())
  end
end

local function _init()
  package.loaded['utils'] = nil
  gpio.mode(_pin, gpio.OUTPUT)

  local c = _utils.doFile('load_config')(_moduleName)
  _set(c.start or '0')
end

_utils.doFile('add_register')(_regName, 'NORMAL', _get, _set)

return _init
