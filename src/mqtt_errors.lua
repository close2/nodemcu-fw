local errors = {}
errors[mqtt.CONN_FAIL_SERVER_NOT_FOUND] = 'There is no broker listening at the specified IP Address and Port';
errors[mqtt.CONN_FAIL_NOT_A_CONNACK_MSG] = 'The response from the broker was not a CONNACK as required by the protocol';
errors[mqtt.CONN_FAIL_DNS] = 'DNS Lookup failed';
errors[mqtt.CONN_FAIL_TIMEOUT_RECEIVING] = 'Timeout waiting for a CONNACK from the broker';
errors[mqtt.CONN_FAIL_TIMEOUT_SENDING] = 'Timeout trying to send the Connect message';
errors[mqtt.CONNACK_ACCEPTED] = 'errors. Note: This will not trigger a failure callback.';
errors[mqtt.CONNACK_REFUSED_PROTOCOL_VER] = 'broker is not a 3.1.1 MQTT broker.';
errors[mqtt.CONNACK_REFUSED_ID_REJECTED] = 'specified ClientID was rejected by the broker. (See  mqtt.Client())';
errors[mqtt.CONNACK_REFUSED_SERVER_UNAVAILABLE] = 'server is unavailable.';
errors[mqtt.CONNACK_REFUSED_BAD_USER_OR_PASS] = 'broker refused the specified username or password.';
errors[mqtt.CONNACK_REFUSED_NOT_AUTHORIZED] = 'username is not authorized.';

return errors
