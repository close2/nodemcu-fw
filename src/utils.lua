local Utils = {}

Utils.doFile = function(f)
  if file.exists(f .. '.lc') then
    return dofile(f .. '.lc')
  else
    return dofile(f .. '.lua')
  end
end

Utils.fileExists = function(f)
  return file.exists(f .. '.lc') or file.exists(f .. '.lua')
end

return Utils