local _utils = require('utils')

local _moduleName = 'mdns_announce'

return function()
  package.loaded['utils'] = nil
  local c = _utils.doFile('load_config')(_moduleName)
  _utils = nil

  local hostname = c.hostname
  local description = c.description

  print('Will now register: ' .. hostname .. ' using mdns.')
  mdns.register(hostname, { description = description, service = 'http', port = 80 })
end
