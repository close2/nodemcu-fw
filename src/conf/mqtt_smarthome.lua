return {
  id       = 'led-' .. node.chipid(),
  server   = '_mqtt._tcp.local',
  port     = '1883',
  secure   = 'false',
  user     = 'mqtt',
  password = 'password'
}
