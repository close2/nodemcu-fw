return 'mdns announce', {
  hostname = {
    displayName = 'hostname',
    desc = 'The device announces itself under this name (don\'t add .local).'
  },
  description = {
    displayName = 'description',
    desc = 'This description is attached to the announcement.'
  }
}
