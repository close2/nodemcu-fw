return 'led', {
  start = {
    displayName = 'start value',
    desc = 'Turn the led on (1) or off (0) when the device starts or reboots.',
    allowed = { '0', '1' }
  }
}