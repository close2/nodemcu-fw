return 'wifi configuration portal', {
  displayName = 'mode',
  desc = 'If mode is "START" the portal is automatically started whenever the devices turns on.\n' ..
         'The portal declares itself as access point (xxxGadget) and allows to configure the wifi parameters.\n'..
         'When in "MANUAL" mode the "set-wifi-button" (if it exists) has to be pressed during boot.',
  allowed = { 'START', 'MANUAL' }
}
