return 'mqtt', {
  id = {
    displayName = 'mqtt prefix',
    desc = 'All functionality will publish/subscribe with this prefix.'
  },
  server = {
    displayName = 'mqtt host',
    desc = 'Address (name or ip-address) of the mqtt server.  (.local services are allowed).'
  },
  port = {
    displayName = 'mqtt port',
    desc = 'The port of the mqtt host.'
  },
  secure = {
    displayName = 'ssl',
    desc = 'Use ssl when connecting to the mqtt host.',
    allowed = { 'true', 'false' }
  },
  user = {
    displayName = 'username',
  },
  password = {
    displayName = 'password',
  }
}


